package com.orders.demo.configuration.security;

import com.orders.demo.common.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SimpleApiKeyFilterTest {

    @Before
    public void setup() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    public void testHasApiKeyWithPerms() throws ServletException, IOException {
        final SimpleApiKeyFilter filter = new SimpleApiKeyFilter();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        when(request.getHeader("x-api-key")).thenReturn("123;admin");
        filter.doFilter(request, response, filterChain);
        final UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) SecurityContextHolder
                .getContext().getAuthentication();
        assertThat(auth.getAuthorities()).hasSize(1);
        assertThat(auth.getAuthorities()).containsExactly(new SimpleGrantedAuthority("admin"));
        assertThat(auth.getPrincipal()).isEqualTo("123");
        assertThat(auth.getDetails()).isEqualTo(new User("123", "admin"));
    }

    @Test
    public void testHasApiKeyWithoutPerms() throws ServletException, IOException {
        final SimpleApiKeyFilter filter = new SimpleApiKeyFilter();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        when(request.getHeader("x-api-key")).thenReturn("123");
        filter.doFilter(request, response, filterChain);
        final UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) SecurityContextHolder
                .getContext().getAuthentication();
        assertThat(auth.getAuthorities()).isEmpty();
        assertThat(auth.getPrincipal()).isEqualTo("123");
        assertThat(auth.getDetails()).isEqualTo(new User("123", null));
    }

    @Test
    public void testNoAuth() throws ServletException, IOException {
        final SimpleApiKeyFilter filter = new SimpleApiKeyFilter();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        when(request.getHeader("x-api-key")).thenReturn(null);
        filter.doFilter(request, response, filterChain);
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    public void testEmptyAuth() throws ServletException, IOException {
        final SimpleApiKeyFilter filter = new SimpleApiKeyFilter();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        when(request.getHeader("x-api-key")).thenReturn(" ");
        filter.doFilter(request, response, filterChain);
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

}
