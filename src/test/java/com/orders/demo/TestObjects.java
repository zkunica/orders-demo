package com.orders.demo;

import com.orders.demo.common.Audit;
import com.orders.demo.common.Currency;
import com.orders.demo.common.CustomerId;
import com.orders.demo.common.Money;
import com.orders.demo.order.Order;
import com.orders.demo.order.OrderCustomerDetails;
import com.orders.demo.product.Product;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TestObjects {

    public static final Product testProductShirt = Product.create(
            Audit.test(),
            "Shirt",
            "Description",
            new Money(BigDecimal.TEN, Currency.EUR));

    public static final Product testProductBag = Product.create(
            Audit.test(),
            "Shirt",
            "Description",
            new Money(BigDecimal.ONE, Currency.EUR));

    public static final Order testOrder = Order.empty(
            Audit.test(),
            new OrderCustomerDetails(new CustomerId("testcustomer"), "test customer", "test@test.com"))
            .addItem(testProductShirt, BigDecimal.ONE)
            .place(Audit.test());

    public static final Order testOrderDifferentCustomer = Order.empty(
            Audit.test(),
            new OrderCustomerDetails(CustomerId.nextId(), "test customer", "test@test.com"))
            .addItem(testProductShirt, BigDecimal.ONE)
            .place(Audit.test());

}
