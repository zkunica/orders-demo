package com.orders.demo.order;

import com.orders.demo.common.Audit;
import com.orders.demo.common.Currency;
import com.orders.demo.common.Money;
import com.orders.demo.product.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class OrderItemTest {

    private final Product testProduct = Product.create(
            Audit.test(),
            "Product",
            "Description",
            new Money(BigDecimal.TEN, Currency.EUR));

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowEmptyProductOnCreateForProduct() {
        OrderItem.createForProduct(null, BigDecimal.ONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowNullQuantityOnCreateForProduct() {
        OrderItem.createForProduct(testProduct, null);
    }

    @Test
    public void shouldCreateFromProductQuantity() {
        final ProductQuantity productQuantity = new ProductQuantity(testProduct, BigDecimal.TEN);
        final OrderItem orderItem = OrderItem.createForProductQuantity(productQuantity);
        assertThat(orderItem.getProduct()).isEqualTo(testProduct);
        assertThat(orderItem.getQuantity()).isEqualTo(BigDecimal.TEN);
        assertThat(orderItem.getTotalAmount())
                .isEqualTo(testProduct.getPrice().multiply(productQuantity.getQuantity()));
    }
}
