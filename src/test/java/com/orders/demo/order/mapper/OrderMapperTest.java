package com.orders.demo.order.mapper;

import com.orders.demo.TestObjects;
import com.orders.demo.order.Order;
import com.orders.demo.order.OrderItem;
import com.orders.demo.order.web.OrderDto;
import com.orders.demo.order.web.OrderItemDto;
import com.orders.demo.common.Audit;
import com.orders.demo.order.jpa.OrderItemJpa;
import com.orders.demo.order.jpa.OrderJpa;
import com.orders.demo.product.mapper.ProductMapper;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class OrderMapperTest {

    private final OrderMapper orderMapper = new OrderMapper(new ProductMapper());

    @Test
    public void shouldMapToJpa() {
        final Order order = TestObjects.testOrder.place(Audit.test());
        final OrderJpa orderJpa = orderMapper.toJpa(order);
        assertThat(orderJpa.getId()).isEqualTo(order.getId().getId());
        assertThat(orderJpa.getAmount()).isEqualTo(order.getTotalAmount().getAmount());
        assertThat(orderJpa.getCurrency()).isEqualTo(order.getTotalAmount().getCurrency().name());
        assertThat(orderJpa.getCreatedAt()).isEqualTo(order.getCreated().getInitiatedAt());
        assertThat(orderJpa.getCreatedBy()).isEqualTo(order.getCreated().getUserId());
        assertThat(orderJpa.getCustomerEmail()).isEqualTo(order.getCustomerDetails().getCustomerEmail());
        assertThat(orderJpa.getCustomerId()).isEqualTo(order.getCustomerDetails().getCustomerId().getId());
        assertThat(orderJpa.getItems()).hasSize(order.getItems().size());
        final OrderItemJpa jpaItem = orderJpa.getItems().get(0);
        final OrderItem modelItem = order.getItems().get(0);
        assertThat(jpaItem.getAmount()).isEqualTo(modelItem.getTotalAmount().getAmount());
        assertThat(jpaItem.getOrder()).isEqualTo(orderJpa);
        assertThat(jpaItem.getProductJpa().getCurrency())
                .isEqualTo(modelItem.getProduct().getPrice().getCurrency().name());
        assertThat(jpaItem.getQuantity()).isEqualTo(modelItem.getQuantity());
        assertThat(jpaItem.getId()).isEqualTo(modelItem.getId().getId());
    }

    @Test
    public void shouldMapToApi() {
        final Order order = TestObjects.testOrder.place(Audit.test());
        final OrderDto orderDto = orderMapper.toApi(order);
        assertThat(orderDto.getId()).isEqualTo(order.getId().getId());
        assertThat(orderDto.getTotalAmount()).isEqualTo(order.getTotalAmount().getAmount());
        assertThat(orderDto.getCurrency()).isEqualTo(order.getTotalAmount().getCurrency().name());
        assertThat(orderDto.getPlacedAt()).isEqualTo(order.getPlaced().getInitiatedAt());
        assertThat(orderDto.getCustomerEmail()).isEqualTo(order.getCustomerDetails().getCustomerEmail());
        final OrderItemDto dtoItem = orderDto.getItems().get(0);
        final OrderItem modelItem = order.getItems().get(0);
        assertThat(dtoItem.getTotalAmount()).isEqualTo(modelItem.getTotalAmount().getAmount());
        Assertions.assertThat(dtoItem.getProduct().getCurrency())
                .isEqualTo(modelItem.getProduct().getPrice().getCurrency().name());
        assertThat(dtoItem.getQuantity()).isEqualTo(modelItem.getQuantity());
        assertThat(dtoItem.getId()).isEqualTo(modelItem.getId().getId());
    }

    @Test
    public void shouldMapJpaToModel() {
        final Order order = TestObjects.testOrder.place(Audit.test());
        final OrderJpa orderJpa = orderMapper.toJpa(order);
        final Order result = orderMapper.toModel(orderJpa);
        assertThat(result).isEqualTo(order);
    }
}
