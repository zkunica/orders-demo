package com.orders.demo.order.web;

import com.orders.demo.common.Audit;
import com.orders.demo.common.CustomerId;
import com.orders.demo.order.CreateOrderCommand;
import com.orders.demo.order.Order;
import com.orders.demo.order.mapper.OrderMapper;
import com.orders.demo.order.OrderNotFoundException;
import com.orders.demo.order.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;

import static com.orders.demo.TestObjects.testOrder;
import static com.orders.demo.TestObjects.testOrderDifferentCustomer;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    private static final String ADMIN_API_KEY = "testadmin;admin";
    private static final String CUSTOMER_API_KEY = "testcustomer;customer";

    @Autowired
    private MockMvc mvc;

    @Autowired
    @Qualifier("jacksonObjectMapper")
    private ObjectMapper objectMapper;

    @MockBean
    private OrderService orderService;

    @MockBean
    private OrderMapper orderMapper;

    @Before
    public void setup() {
    }

    @Test
    public void shouldGetOrdersAsCustomer() throws Exception {
        final Order order = testOrder;
        given(orderService.findAllOrdersByCustomerId(
                any(CustomerId.class),
                any(Instant.class),
                any(Instant.class))).willReturn(Collections.singletonList(order));
        given(orderMapper.toApi(order)).willCallRealMethod();
        mvc.perform(get("/orders")
                .header("x-api-key", CUSTOMER_API_KEY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(order.getId().getId())))
                .andExpect(jsonPath("$[0].totalAmount", is(order.getTotalAmount().getAmount().doubleValue())))
                .andExpect(jsonPath("$[0].currency", is(order.getTotalAmount().getCurrency().toString())));
        verify(orderService, never()).findAllOrders(any(Instant.class), any(Instant.class));
    }

    @Test
    public void shouldGetOrdersAsAdmin() throws Exception {
        final Order order = testOrder;
        given(orderService.findAllOrders(
                any(Instant.class),
                any(Instant.class))).willReturn(Collections.singletonList(order));
        given(orderMapper.toApi(order)).willCallRealMethod();
        mvc.perform(get("/orders")
                .header("x-api-key", ADMIN_API_KEY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(order.getId().getId())))
                .andExpect(jsonPath("$[0].totalAmount", is(order.getTotalAmount().getAmount().doubleValue())))
                .andExpect(jsonPath("$[0].currency", is(order.getTotalAmount().getCurrency().toString())));
        verify(orderService, never())
                .findAllOrdersByCustomerId(any(CustomerId.class), any(Instant.class), any(Instant.class));
    }

    @Test
    public void shouldCreateOwnOrderAsCustomer() throws Exception {
        final CreateOrderRequest createOrderRequest = new CreateOrderRequest(
                "testcustomer",
                "test customer",
                "test@test.com",
                Arrays.asList(new CreateOrderItemRequest("1", BigDecimal.TEN)));
        final ArgumentCaptor<CreateOrderCommand> createOrderCommandArgumentCaptor = ArgumentCaptor
                .forClass(CreateOrderCommand.class);
        given(orderService.placeOrder(any(Audit.class), createOrderCommandArgumentCaptor.capture()))
                .willReturn(testOrder);
        given(orderMapper.toApi(testOrder)).willCallRealMethod();
        mvc.perform(post("/orders")
                .header("x-api-key", CUSTOMER_API_KEY)
                .content(objectMapper.writeValueAsString(createOrderRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(testOrder.getId().getId())))
                .andExpect(jsonPath("$.totalAmount", is(testOrder.getTotalAmount().getAmount().doubleValue())))
                .andExpect(jsonPath("$.currency", is(testOrder.getTotalAmount().getCurrency().toString())));
        final CreateOrderCommand captured = createOrderCommandArgumentCaptor.getValue();
        assertThat(captured.getCustomerId().getId()).isEqualTo(createOrderRequest.getCustomerId());
        assertThat(captured.getCustomerEmail()).isEqualTo(createOrderRequest.getCustomerEmail());
        assertThat(captured.getCustomerName()).isEqualTo(createOrderRequest.getCustomerName());
        assertThat(captured.getItems()).hasSize(1);
        assertThat(captured.getItems().get(0).getProductId().getId())
                .isEqualTo(createOrderRequest.getItems().get(0).getProductId());
        assertThat(captured.getItems().get(0).getQuantity())
                .isEqualTo(createOrderRequest.getItems().get(0).getQuantity());
    }

    @Test
    public void shouldCreateOrder() throws Exception {
        final CreateOrderRequest createOrderRequest = new CreateOrderRequest(
                "someOtherCustomer",
                "test customer",
                "test@test.com",
                Arrays.asList(new CreateOrderItemRequest("1", BigDecimal.TEN)));
        final ArgumentCaptor<CreateOrderCommand> createOrderCommandArgumentCaptor = ArgumentCaptor
                .forClass(CreateOrderCommand.class);
        given(orderService.placeOrder(any(Audit.class), createOrderCommandArgumentCaptor.capture()))
                .willReturn(testOrder);
        given(orderMapper.toApi(testOrder)).willCallRealMethod();
        mvc.perform(post("/orders")
                .header("x-api-key", ADMIN_API_KEY)
                .content(objectMapper.writeValueAsString(createOrderRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(testOrder.getId().getId())))
                .andExpect(jsonPath("$.totalAmount", is(testOrder.getTotalAmount().getAmount().doubleValue())))
                .andExpect(jsonPath("$.currency", is(testOrder.getTotalAmount().getCurrency().toString())));
        final CreateOrderCommand captured = createOrderCommandArgumentCaptor.getValue();
        assertThat(captured.getCustomerId().getId()).isEqualTo(createOrderRequest.getCustomerId());
        assertThat(captured.getCustomerEmail()).isEqualTo(createOrderRequest.getCustomerEmail());
        assertThat(captured.getCustomerName()).isEqualTo(createOrderRequest.getCustomerName());
        assertThat(captured.getItems()).hasSize(1);
        assertThat(captured.getItems().get(0).getProductId().getId())
                .isEqualTo(createOrderRequest.getItems().get(0).getProductId());
        assertThat(captured.getItems().get(0).getQuantity())
                .isEqualTo(createOrderRequest.getItems().get(0).getQuantity());
    }

    @Test
    public void shouldNotAllowCreateOtherOrderAsCustomer() throws Exception {
        final CreateOrderRequest createOrderRequest = new CreateOrderRequest(
                "someOtherCustomer",
                "test customer",
                "test@test.com",
                Arrays.asList(new CreateOrderItemRequest("1", BigDecimal.TEN)));
        mvc.perform(post("/orders")
                .header("x-api-key", CUSTOMER_API_KEY)
                .content(objectMapper.writeValueAsString(createOrderRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldGetOrderAsAdmin() throws Exception {
        final Order order = testOrder;
        given(orderService.getOrder(order.getId())).willReturn(order);
        given(orderMapper.toApi(order)).willCallRealMethod();
        mvc.perform(get("/orders/" + order.getId().getId())
                .header("x-api-key", ADMIN_API_KEY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(order.getId().getId())))
                .andExpect(jsonPath("$.totalAmount", is(order.getTotalAmount().getAmount().doubleValue())))
                .andExpect(jsonPath("$.currency", is(order.getTotalAmount().getCurrency().toString())));
    }

    @Test
    public void shouldGetOrderAsCustomer() throws Exception {
        final Order order = testOrder;
        given(orderService.getOrderByIdAndCustomerId(order.getId(), new CustomerId("testcustomer"))).willReturn(order);
        given(orderMapper.toApi(order)).willCallRealMethod();
        mvc.perform(get("/orders/" + order.getId().getId())
                .header("x-api-key", CUSTOMER_API_KEY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(order.getId().getId())))
                .andExpect(jsonPath("$.totalAmount", is(order.getTotalAmount().getAmount().doubleValue())))
                .andExpect(jsonPath("$.currency", is(order.getTotalAmount().getCurrency().toString())));
    }

    @Test
    public void shouldNotGetOrderAsCustomerForDifferentCustomer() throws Exception {
        final Order order = testOrderDifferentCustomer;
        given(orderService.getOrderByIdAndCustomerId(order.getId(), new CustomerId("testcustomer")))
                .willThrow(new OrderNotFoundException(order.getId()));
        given(orderMapper.toApi(order)).willCallRealMethod();
        mvc.perform(get("/orders/" + order.getId().getId())
                .header("x-api-key", CUSTOMER_API_KEY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

}
