package com.orders.demo.order;

import com.orders.demo.common.Audit;
import com.orders.demo.common.Currency;
import com.orders.demo.common.CustomerId;
import com.orders.demo.common.Money;
import com.orders.demo.product.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class OrderTest {

    private final CustomerId customerId = CustomerId.nextId();
    private final Product firstProduct = Product.create(
            Audit.test(),
            "Product",
            "Description",
            new Money(BigDecimal.TEN, Currency.EUR));
    private final Product secondProduct = Product.create(
            Audit.test(),
            "Product 2",
            "Description 2",
            new Money(BigDecimal.ONE, Currency.EUR));

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowEmptyCustomer() {
        Order.empty(Audit.test(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowEmptyAudit() {
        Order.empty(null, orderCustomerDetails());
    }

    @Test
    public void shouldCreateEmptyOrder() {
        final Order order = Order.empty(Audit.test(), orderCustomerDetails());
        assertThat(order.getCustomerDetails()).isEqualTo(orderCustomerDetails());
        assertThat(order.getId()).isNotNull();
        assertThat(order.getCreated()).isEqualTo(Audit.test());
    }

    @Test
    public void shouldAddItem() {
        final Order order = Order
                .empty(Audit.test(), orderCustomerDetails())
                .addItem(firstProduct, BigDecimal.TEN);
        assertThat(order.getItems()).hasSize(1);
        final OrderItem orderItem = order.getItems().get(0);
        assertThat(orderItem.getId()).isNotNull();
        assertThat(orderItem.getQuantity()).isEqualTo(BigDecimal.TEN);
        assertThat(orderItem.getProduct()).isEqualTo(firstProduct);
        assertThat(orderItem.getTotalAmount()).isEqualTo(firstProduct.getPrice().multiply(BigDecimal.TEN));
    }

    @Test
    public void shouldAddItems() {
        final Order order = Order
                .empty(Audit.test(), orderCustomerDetails())
                .addItems(Arrays.asList(
                        new ProductQuantity(firstProduct, BigDecimal.ONE),
                        new ProductQuantity(secondProduct, BigDecimal.TEN)));
        assertThat(order.getItems()).hasSize(2);
        assertThat(order.getTotalAmount()).isEqualTo(firstProduct
                .getPrice()
                .multiply(BigDecimal.ONE)
                .add(secondProduct.getPrice().multiply(BigDecimal.TEN)));
    }

    @Test
    public void shouldPlaceOrder() {
        final Order order = Order
                .empty(Audit.test(), orderCustomerDetails())
                .addItem(firstProduct, BigDecimal.ONE)
                .place(Audit.test());
        assertThat(order.getStatus()).isEqualTo(OrderStatus.PLACED);
        assertThat(order.getPlaced()).isEqualTo(Audit.test());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotPlaceEmptyOrder() {
        Order.empty(Audit.test(), orderCustomerDetails())
                .place(Audit.test());
    }

    private OrderCustomerDetails orderCustomerDetails() {
        return new OrderCustomerDetails(
                customerId,
                "test",
                "test@test.com"
        );
    }

}
