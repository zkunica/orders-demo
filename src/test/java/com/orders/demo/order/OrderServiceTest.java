package com.orders.demo.order;

import com.orders.demo.TestObjects;
import com.orders.demo.common.Audit;
import com.orders.demo.common.CustomerId;
import com.orders.demo.order.jpa.OrderJpa;
import com.orders.demo.order.jpa.OrderRepository;
import com.orders.demo.order.mapper.OrderMapper;
import com.orders.demo.product.Product;
import com.orders.demo.product.ProductId;
import com.orders.demo.product.mapper.ProductMapper;
import com.orders.demo.product.ProductNotFoundException;
import com.orders.demo.product.jpa.ProductJpa;
import com.orders.demo.product.jpa.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private OrderMapper orderMapper;

    @Mock
    private ProductMapper productMapper;

    private OrderService orderService;

    @Before
    public void setup() {
        orderService = new OrderService(orderRepository, productRepository, orderMapper, productMapper);
    }

    @Test
    public void shouldGetAllOrders() {
        final OrderJpa orderJpa = mock(OrderJpa.class);
        final Instant from = Instant.now().minusSeconds(100);
        final Instant to = Instant.now();
        when(orderRepository.findAllByPlacedAtBetween(from, to)).thenReturn(Stream.of(orderJpa));
        when(orderMapper.toModel(orderJpa)).thenReturn(TestObjects.testOrder);
        orderService.findAllOrders(from, to);
        verify(orderRepository, times(1)).findAllByPlacedAtBetween(from, to);
        verify(orderMapper, times(1)).toModel(orderJpa);
    }

    @Test
    public void shouldGetAllOrdersByCustomerId() {
        final CustomerId customerId = CustomerId.nextId();
        final OrderJpa orderJpa = mock(OrderJpa.class);
        final Instant from = Instant.now().minusSeconds(100);
        final Instant to = Instant.now();
        when(orderRepository.findAllByCustomerIdAndPlacedAtBetween(customerId.getId(), from, to))
                .thenReturn(Stream.of(orderJpa));
        when(orderMapper.toModel(orderJpa)).thenReturn(TestObjects.testOrder);
        orderService.findAllOrdersByCustomerId(customerId, from, to);
        verify(orderRepository, times(1)).findAllByCustomerIdAndPlacedAtBetween(customerId.getId(), from, to);
        verify(orderMapper, times(1)).toModel(orderJpa);
    }

    @Test
    public void shouldPlaceOrder() {
        final CreateOrderCommand createOrderCommand = new CreateOrderCommand(
                CustomerId.nextId(),
                "test customer",
                "test@test.com",
                Arrays.asList(new CreateOrderItemCommand(
                        ProductId.nextId(),
                        BigDecimal.TEN))
        );
        final Product testProduct = TestObjects.testProductShirt;
        final Order testOrder = Order.empty(Audit.test(), new OrderCustomerDetails(CustomerId.nextId(), "", ""));
        final OrderJpa testOrderJpa = mock(OrderJpa.class);
        final ProductJpa testProductJpa = mock(ProductJpa.class);
        final ArgumentCaptor<OrderJpa> orderJpaArgumentCaptor = ArgumentCaptor.forClass(OrderJpa.class);
        when(orderRepository.save(orderJpaArgumentCaptor.capture())).thenReturn(testOrderJpa);
        when(productRepository.findByOrOriginalIdAndActiveIsTrue(anyString())).thenReturn(Optional.of(testProductJpa));
        when(productMapper.toModel(testProductJpa)).thenReturn(testProduct);
        when(orderMapper.toJpa(any(Order.class))).thenReturn(testOrderJpa);
        when(orderMapper.toModel(testOrderJpa)).thenReturn(testOrder);
        orderService.placeOrder(Audit.test(), createOrderCommand);
        verify(orderRepository, times(1)).save(testOrderJpa);
        final OrderJpa capturedOrderJpa = orderJpaArgumentCaptor.getValue();
        assertThat(capturedOrderJpa).isEqualTo(testOrderJpa);
    }

    @Test(expected = ProductNotFoundException.class)
    public void shouldFailPlaceOrderIfProductNotFound() {
        final CreateOrderCommand createOrderRequest = new CreateOrderCommand(
                CustomerId.nextId(),
                "test customer",
                "test@test.com",
                Arrays.asList(new CreateOrderItemCommand(
                        ProductId.nextId(),
                        BigDecimal.TEN))
        );
        when(productRepository.findByOrOriginalIdAndActiveIsTrue(anyString())).thenReturn(Optional.empty());
        orderService.placeOrder(Audit.test(), createOrderRequest);
    }

    @Test
    public void shouldGetOrderById() {
        final OrderId orderId = OrderId.nextId();
        final OrderJpa orderJpa = mock(OrderJpa.class);
        final Instant from = Instant.now().minusSeconds(100);
        final Instant to = Instant.now();
        when(orderRepository.findById(orderId.getId())).thenReturn(Optional.of(orderJpa));
        when(orderMapper.toModel(orderJpa)).thenReturn(TestObjects.testOrder);
        orderService.getOrder(orderId);
        verify(orderRepository, times(1)).findById(orderId.getId());
        verify(orderMapper, times(1)).toModel(orderJpa);
    }

    @Test
    public void shouldGetOrderByIdAndUserId() {
        final OrderId orderId = OrderId.nextId();
        final CustomerId customerId = CustomerId.nextId();
        final OrderJpa orderJpa = mock(OrderJpa.class);
        final Instant from = Instant.now().minusSeconds(100);
        final Instant to = Instant.now();
        when(orderRepository.findOrderJpaByIdAndCustomerId(orderId.getId(), customerId.getId())).thenReturn(Optional.of(orderJpa));
        when(orderMapper.toModel(orderJpa)).thenReturn(TestObjects.testOrder);
        orderService.getOrderByIdAndCustomerId(orderId, customerId);
        verify(orderRepository, times(1)).findOrderJpaByIdAndCustomerId(orderId.getId(), customerId.getId());
        verify(orderMapper, times(1)).toModel(orderJpa);
    }
}
