package com.orders.demo.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MoneyTest {

    @Test
    public void shouldRoundToCurrency() {
        final Money initial = new Money(BigDecimal.valueOf(345.678), Currency.EUR);
        assertThat(initial.getAmount()).isEqualTo(new BigDecimal("345.68"));
    }

    @Test
    public void shouldAllowAddOnEmpty() {
        final Money initial = new Money(null, null);
        final Money updated = initial.add(new Money(BigDecimal.TEN, Currency.EUR));
        assertThat(updated.getAmount()).isEqualTo(new BigDecimal("10.00"));
        assertThat(updated.getCurrency()).isEqualTo(Currency.EUR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowMultiplyOnEmpty() {
        final Money initial = new Money(null, null);
        initial.multiply(BigDecimal.TEN);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowAddOnDifferentCurrency() {
        final Money initial = new Money(BigDecimal.TEN, Currency.USD);
        initial.add(new Money(BigDecimal.TEN, Currency.EUR));
    }
}
