package com.orders.demo.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyTest {

    @Test
    public void shouldHaveCorrectScale() {
        assertThat(Currency.EUR.getScale()).isEqualTo(2);
        assertThat(Currency.USD.getScale()).isEqualTo(2);
        assertThat(Currency.HUF.getScale()).isEqualTo(0);
    }

}
