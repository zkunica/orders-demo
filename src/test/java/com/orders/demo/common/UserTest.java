package com.orders.demo.common;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserTest {

    @Test
    public void shouldDetermineAdmin() {
        final User user = new User("1", "admin,test");
        Assertions.assertThat(user.isAdmin()).isTrue();
        Assertions.assertThat(user.isCustomer()).isFalse();
    }

    @Test
    public void shouldDetermineCustomer() {
        final User user = new User("1", "customer");
        Assertions.assertThat(user.isAdmin()).isFalse();
        Assertions.assertThat(user.isCustomer()).isTrue();
    }

}
