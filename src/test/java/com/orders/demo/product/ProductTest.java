package com.orders.demo.product;

import com.orders.demo.common.Audit;
import com.orders.demo.common.Currency;
import com.orders.demo.common.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ProductTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowEmptyName() {
        Product.create(Audit.test(), null, "description", Money.empty());
    }

    @Test
    public void shouldUpdateProduct() {
        final Product initial = Product.create(
                Audit.test(),
                "name",
                "description",
                testPrice());
        final ProductId productId = initial.getId();
        final Product updated = initial.update(
                Audit.test(),
                "new name",
                "new description",
                testPrice());
        assertThat(updated.getName()).isEqualTo("new name");
        assertThat(updated.getDescription()).isEqualTo("new description");
        assertThat(updated.getPrice()).isEqualTo(new Money(BigDecimal.TEN, Currency.EUR));
        assertThat(updated.getId()).isNotEqualTo(productId);
        assertThat(updated.getOriginalId()).isEqualTo(productId);
        assertThat(updated.getActive()).isTrue();
    }

    @Test
    public void shouldDeactivateProduct() {
        final Product initial = Product.create(
                Audit.test(),
                "name",
                "description",
                testPrice());
        final ProductId productId = initial.getId();
        final Product updated = initial.deactivate();
        assertThat(updated.getActive()).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowNegativePrice() {
        final Product initial = Product.create(
                Audit.test(),
                "name",
                "description",
                new Money(BigDecimal.valueOf(-1.0), Currency.EUR));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowZeroPrice() {
        final Product initial = Product.create(
                Audit.test(),
                "name",
                "description",
                new Money(BigDecimal.ZERO, Currency.EUR));
    }

    private Money testPrice() {
        return new Money(BigDecimal.TEN, Currency.EUR);
    }

}
