package com.orders.demo.product;

import com.orders.demo.TestObjects;
import com.orders.demo.common.Audit;
import com.orders.demo.common.Currency;
import com.orders.demo.common.Money;
import com.orders.demo.product.jpa.ProductJpa;
import com.orders.demo.product.jpa.ProductRepository;
import com.orders.demo.product.mapper.ProductMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductMapper productMapper;

    private ProductService productService;

    @Before
    public void setup() {
        productService = new ProductService(productRepository, productMapper);
    }

    @Test
    public void shouldBuyProduct() {
        final Audit audit = Audit.test();
        final CreateProductCommand command = new CreateProductCommand("test name", "description",
                new Money(BigDecimal.TEN, Currency.EUR));
        final ArgumentCaptor<ProductJpa> productArgumentCaptor = ArgumentCaptor.forClass(ProductJpa.class);
        when(productMapper.toModel(any(ProductJpa.class))).thenCallRealMethod();
        when(productMapper.toJpa(any(Product.class))).thenCallRealMethod();
        when(productRepository.save(productArgumentCaptor.capture()))
                .thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        final Product saved = productService.createProduct(audit, command);
        verify(productRepository, times(1)).save(productArgumentCaptor.capture());
        final ProductJpa captured = productArgumentCaptor.getValue();
        assertThat(captured.getAmount()).isEqualTo(new BigDecimal("10.00"));
        assertThat(captured.getCurrency()).isEqualTo("EUR");
        assertThat(captured.getCreatedAt()).isEqualTo(audit.getInitiatedAt());
        assertThat(captured.getName()).isEqualTo("test name");
        assertThat(captured.getDescription()).isEqualTo("description");
    }

    @Test
    public void shouldUpdateProduct() {
        final ProductId productId = ProductId.nextId();
        final ProductJpa existing = new ProductJpa(
                productId.getId(),
                productId.getId(),
                "test",
                "test",
                BigDecimal.ONE,
                "EUR",
                true,
                Instant.now(),
                "test"
        );
        final Audit audit = Audit.test();
        final ModifyProductCommand command = new ModifyProductCommand("test name", "description",
                new Money(BigDecimal.TEN, Currency.EUR));
        final ArgumentCaptor<ProductJpa> productArgumentCaptor = ArgumentCaptor.forClass(ProductJpa.class);
        when(productMapper.toModel(any(ProductJpa.class))).thenCallRealMethod();
        when(productMapper.toJpa(any(Product.class))).thenCallRealMethod();
        when(productRepository.findByOrOriginalIdAndActiveIsTrue(productId.getId())).thenReturn(Optional.of(existing));
        when(productRepository.save(productArgumentCaptor.capture()))
                .thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        final Product saved = productService.updateProduct(audit, productId, command);
        verify(productRepository, times(2)).save(productArgumentCaptor.capture());
        final ProductJpa capturedOldVersion = productArgumentCaptor.getAllValues().get(0);
        assertThat(capturedOldVersion.getAmount()).isEqualTo(new BigDecimal("1.00"));
        assertThat(capturedOldVersion.getCurrency()).isEqualTo("EUR");
        assertThat(capturedOldVersion.getName()).isEqualTo("test");
        assertThat(capturedOldVersion.getDescription()).isEqualTo("test");
        assertThat(capturedOldVersion.getActive()).isEqualTo(false);
        final ProductJpa capturedNewVersion = productArgumentCaptor.getAllValues().get(1);
        assertThat(capturedNewVersion.getAmount()).isEqualTo(new BigDecimal("10.00"));
        assertThat(capturedNewVersion.getCurrency()).isEqualTo("EUR");
        assertThat(capturedNewVersion.getName()).isEqualTo("test name");
        assertThat(capturedNewVersion.getDescription()).isEqualTo("description");
        assertThat(capturedNewVersion.getActive()).isEqualTo(true);
    }

    @Test(expected = ProductNotFoundException.class)
    public void shouldFailToUpdateIfDoesntExist() {
        final ProductId productId = ProductId.nextId();
        final Audit audit = Audit.test();
        final ModifyProductCommand modifyProductCommand = new ModifyProductCommand("test name", "description",
                new Money(BigDecimal.TEN, Currency.EUR));
        when(productRepository.findByOrOriginalIdAndActiveIsTrue(productId.getId())).thenReturn(Optional.empty());
        productService.updateProduct(audit, productId, modifyProductCommand);
        verify(productRepository, never()).save(any(ProductJpa.class));
    }

    @Test
    public void shouldGetProductById() {
        final Audit audit = Audit.test();
        final Product product = TestObjects.testProductBag;
        final ProductJpa productJpa = mock(ProductJpa.class);
        when(productRepository.findById(product.getId().getId())).thenReturn(Optional.of(productJpa));
        when(productMapper.toModel(productJpa)).thenReturn(product);
        productService.getProduct(audit, product.getId());
        verify(productRepository, times(1)).findById(product.getId().getId());
        verify(productMapper, times(1)).toModel(productJpa);
    }

    @Test(expected = ProductNotFoundException.class)
    public void shouldFailGetProductByIdNotFound() {
        final Audit audit = Audit.test();
        final Product product = TestObjects.testProductBag;
        final ProductJpa productJpa = mock(ProductJpa.class);
        when(productRepository.findById(product.getId().getId())).thenReturn(Optional.empty());
        productService.getProduct(audit, product.getId());
    }

    @Test
    public void shouldGetActiveProductByOriginalId() {
        final Audit audit = Audit.test();
        final Product product = TestObjects.testProductBag;
        final ProductJpa productJpa = mock(ProductJpa.class);
        when(productRepository.findByOrOriginalIdAndActiveIsTrue(product.getId().getId()))
                .thenReturn(Optional.of(productJpa));
        when(productMapper.toModel(productJpa)).thenReturn(product);
        productService.getActiveProductByOriginalId(audit, product.getId());
        verify(productRepository, times(1)).findByOrOriginalIdAndActiveIsTrue(product.getId().getId());
        verify(productMapper, times(1)).toModel(productJpa);
    }

    @Test(expected = ProductNotFoundException.class)
    public void shouldFailGetActiveProductByOriginalId() {
        final Audit audit = Audit.test();
        final Product product = TestObjects.testProductBag;
        when(productRepository.findByOrOriginalIdAndActiveIsTrue(product.getId().getId())).thenReturn(Optional.empty());
        productService.getActiveProductByOriginalId(audit, product.getId());
    }

    @Test
    public void shouldFindAllProducts() {
        final Product product = TestObjects.testProductBag;
        final ProductJpa productJpa = mock(ProductJpa.class);
        when(productRepository.findAllProducts()).thenReturn(Stream.of(productJpa));
        when(productMapper.toModel(productJpa)).thenReturn(product);
        productService.findAllProducts();
        verify(productRepository, times(1)).findAllProducts();
        verify(productMapper, times(1)).toModel(productJpa);
    }
}
