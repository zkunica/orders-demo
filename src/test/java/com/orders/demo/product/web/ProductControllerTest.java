package com.orders.demo.product.web;

import com.orders.demo.common.Audit;
import com.orders.demo.common.Currency;
import com.orders.demo.common.Money;
import com.orders.demo.product.CreateProductCommand;
import com.orders.demo.product.ModifyProductCommand;
import com.orders.demo.product.Product;
import com.orders.demo.product.ProductId;
import com.orders.demo.product.mapper.ProductMapper;
import com.orders.demo.product.ProductNotFoundException;
import com.orders.demo.product.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;

import static com.orders.demo.TestObjects.testProductBag;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    private static final String ADMIN_API_KEY = "testadmin;admin";
    private static final String CUSTOMER_API_KEY = "testcustomer;customer";

    @Autowired
    private MockMvc mvc;

    @Qualifier("jacksonObjectMapper")
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ProductService productService;

    @MockBean
    private ProductMapper productMapper;

    @Before
    public void setup() {
    }

    @Test
    public void shouldGetProducts() throws Exception {
        final ProductId productId = ProductId.nextId();
        final Product product = new Product(
                productId,
                productId,
                "test",
                "test",
                new Money(BigDecimal.TEN, Currency.EUR),
                Audit.test(),
                true);
        given(productService.findAllProducts()).willReturn(Collections.singletonList(product));
        given(productMapper.toApi(any(Product.class))).willCallRealMethod();
        mvc.perform(get("/products")
                .header("x-api-key", CUSTOMER_API_KEY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(product.getId().getId())))
                .andExpect(jsonPath("$[0].price", is(product.getPrice().getAmount().doubleValue())))
                .andExpect(jsonPath("$[0].currency", is(product.getPrice().getCurrency().toString())))
                .andExpect(jsonPath("$[0].name", is(product.getName())));
    }

    @Test
    public void shouldCreateProduct() throws Exception {
        final Audit audit = Audit.test();
        final CreateProductRequest request = new CreateProductRequest(
                "test",
                "test",
                BigDecimal.TEN,
                Currency.EUR.name());
        final ProductId productId = ProductId.nextId();
        final Product product = new Product(
                productId,
                productId,
                "test",
                "test",
                new Money(BigDecimal.TEN, Currency.EUR),
                audit,
                true);
        final ArgumentCaptor<CreateProductCommand> commandArgumentCaptor = ArgumentCaptor
                .forClass(CreateProductCommand.class);
        given(productService.createProduct(any(Audit.class), commandArgumentCaptor.capture())).willReturn(product);
        given(productMapper.toApi(any(Product.class))).willCallRealMethod();
        mvc.perform(post("/products")
                .header("x-api-key", ADMIN_API_KEY)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(product.getId().getId())))
                .andExpect(jsonPath("$.price", is(product.getPrice().getAmount().doubleValue())))
                .andExpect(jsonPath("$.currency", is(product.getPrice().getCurrency().name())))
                .andExpect(jsonPath("$.name", is(product.getName())));
        final CreateProductCommand capturedCommand = commandArgumentCaptor.getValue();
        assertThat(capturedCommand.getPrice().getAmount()).isEqualTo(request.getPrice().setScale(2, RoundingMode.HALF_UP));
        assertThat(capturedCommand.getPrice().getCurrency().name()).isEqualTo(request.getCurrency());
        assertThat(capturedCommand.getDescription()).isEqualTo(request.getDescription());
        assertThat(capturedCommand.getName()).isEqualTo(request.getName());
    }

    @Test
    public void shouldUpdateProduct() throws Exception {
        final Audit audit = Audit.test();
        final CreateProductRequest request = new CreateProductRequest(
                "test2",
                "test",
                BigDecimal.ONE,
                Currency.EUR.name());
        final ProductId productId = ProductId.nextId();
        final Product product = new Product(
                productId,
                productId,
                "test",
                "test",
                new Money(BigDecimal.TEN, Currency.EUR),
                audit,
                true);
        final ArgumentCaptor<ModifyProductCommand> commandArgumentCaptor = ArgumentCaptor
                .forClass(ModifyProductCommand.class);
        final ArgumentCaptor<ProductId> productIdArgumentCaptor = ArgumentCaptor.forClass(ProductId.class);
        given(productService
                .updateProduct(any(Audit.class), productIdArgumentCaptor.capture(), commandArgumentCaptor.capture()))
                .willReturn(product);
        given(productMapper.toApi(any(Product.class))).willCallRealMethod();
        mvc.perform(put("/products/" + product.getId().getId())
                .header("x-api-key", ADMIN_API_KEY)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(product.getId().getId())))
                .andExpect(jsonPath("$.price", is(product.getPrice().getAmount().doubleValue())))
                .andExpect(jsonPath("$.currency", is(product.getPrice().getCurrency().name())))
                .andExpect(jsonPath("$.name", is(product.getName())));
        final ModifyProductCommand capturedCommand = commandArgumentCaptor.getValue();
        assertThat(capturedCommand.getPrice().getCurrency().name()).isEqualTo(request.getCurrency());
        assertThat(capturedCommand.getPrice().getAmount()).isEqualTo(request.getPrice().setScale(2, RoundingMode.HALF_UP));
        assertThat(capturedCommand.getDescription()).isEqualTo(request.getDescription());
        assertThat(capturedCommand.getName()).isEqualTo(request.getName());
        final ProductId capturedProductId = productIdArgumentCaptor.getValue();
        assertThat(capturedProductId).isEqualTo(product.getId());
    }

    @Test
    public void shouldFailIfNotFound() throws Exception {
        final Audit audit = Audit.test();
        final CreateProductRequest request = new CreateProductRequest(
                "test2",
                "test",
                BigDecimal.ONE,
                Currency.EUR.name());
        final ArgumentCaptor<ModifyProductCommand> requestArgumentCaptor = ArgumentCaptor
                .forClass(ModifyProductCommand.class);
        final ArgumentCaptor<ProductId> productIdArgumentCaptor = ArgumentCaptor.forClass(ProductId.class);
        given(productService
                .updateProduct(any(Audit.class), productIdArgumentCaptor.capture(), requestArgumentCaptor.capture()))
                .willThrow(new ProductNotFoundException(ProductId.nextId()));
        mvc.perform(put("/products/123")
                .header("x-api-key", ADMIN_API_KEY)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldFailIfInvalidBody() throws Exception {
        final NonValidatedCreateProductRequest request = new NonValidatedCreateProductRequest(
                null,
                "test",
                BigDecimal.ONE,
                Currency.EUR.name());
        mvc.perform(post("/products")
                .header("x-api-key", ADMIN_API_KEY)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldGetProductAsCustomer() throws Exception {
        final Product product = testProductBag;
        final ArgumentCaptor<ProductId> productIdArgumentCaptor = ArgumentCaptor.forClass(ProductId.class);
        given(productService.getActiveProductByOriginalId(any(Audit.class), productIdArgumentCaptor.capture())).willReturn(product);
        given(productMapper.toApi(product)).willCallRealMethod();
        mvc.perform(get("/products/" + product.getId().getId())
                .header("x-api-key", CUSTOMER_API_KEY)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(product.getId().getId())))
                .andExpect(jsonPath("$.name", is(product.getName())))
                .andExpect(jsonPath("$.currency", is(product.getPrice().getCurrency().toString())))
                .andExpect(jsonPath("$.price", is(product.getPrice().getAmount().doubleValue())));
        assertThat(productIdArgumentCaptor.getValue()).isEqualTo(product.getId());
    }

}
