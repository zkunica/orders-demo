package com.orders.demo.product.web;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class NonValidatedCreateProductRequest {

    String name;
    String description;
    BigDecimal price;
    String currency;
}
