package com.orders.demo.product.mapper;

import com.orders.demo.TestObjects;
import com.orders.demo.product.Product;
import com.orders.demo.product.web.ProductDto;
import com.orders.demo.product.jpa.ProductJpa;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ProductMapperTest {

    private final ProductMapper productMapper = new ProductMapper();

    @Test
    public void shouldMapToJpa() {
        final Product product = TestObjects.testProductBag;
        final ProductJpa productJpa = productMapper.toJpa(product);
        assertThat(productJpa.getActive()).isTrue();
        assertThat(productJpa.getId()).isEqualTo(product.getId().getId());
        assertThat(productJpa.getName()).isEqualTo(product.getName());
        assertThat(productJpa.getDescription()).isEqualTo(product.getDescription());
        assertThat(productJpa.getAmount()).isEqualTo(product.getPrice().getAmount());
        assertThat(productJpa.getCurrency()).isEqualTo(product.getPrice().getCurrency().name());
    }

    @Test
    public void shouldMapToApi() {
        final Product product = TestObjects.testProductBag;
        final ProductDto productDto = productMapper.toApi(product);
        assertThat(productDto.getId()).isEqualTo(product.getId().getId());
        assertThat(productDto.getName()).isEqualTo(product.getName());
        assertThat(productDto.getDescription()).isEqualTo(product.getDescription());
        assertThat(productDto.getPrice()).isEqualTo(product.getPrice().getAmount());
        assertThat(productDto.getCurrency()).isEqualTo(product.getPrice().getCurrency().name());
    }

    @Test
    public void shouldMapJpaToModel() {
        final Product product = TestObjects.testProductBag;
        final ProductJpa productJpa = productMapper.toJpa(product);
        final Product result = productMapper.toModel(productJpa);
        assertThat(result).isEqualTo(product);
    }
}
