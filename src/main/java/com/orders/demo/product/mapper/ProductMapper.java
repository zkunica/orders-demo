package com.orders.demo.product.mapper;

import com.orders.demo.common.Audit;
import com.orders.demo.common.Currency;
import com.orders.demo.common.Money;
import com.orders.demo.product.Product;
import com.orders.demo.product.ProductId;
import com.orders.demo.product.jpa.ProductJpa;
import com.orders.demo.product.web.ProductDto;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    public ProductJpa toJpa(final Product model) {
        return new ProductJpa(
                model.getId().getId(),
                model.getOriginalId().getId(),
                model.getName(),
                model.getDescription(),
                model.getPrice().getAmount(),
                model.getPrice().getCurrency().name(),
                model.getActive(),
                model.getCreated().getInitiatedAt(),
                model.getCreated().getUserId()
        );
    }

    public ProductDto toApi(final Product model) {
        return new ProductDto(
                model.getOriginalId().getId(),
                model.getName(),
                model.getDescription(),
                model.getPrice().getAmount(),
                model.getPrice().getCurrency().name());
    }

    public Product toModel(final ProductJpa jpa) {
        return new Product(
                new ProductId(jpa.getId()),
                new ProductId(jpa.getOriginalId()),
                jpa.getName(),
                jpa.getDescription(),
                new Money(jpa.getAmount(), Currency.valueOf(jpa.getCurrency())),
                new Audit(jpa.getCreatedBy(), jpa.getCreatedAt()),
                jpa.getActive()
        );
    }
}
