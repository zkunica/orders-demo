package com.orders.demo.product.web;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import java.math.BigDecimal;

@ApiModel(description = "Product.")
@Value
public final class ProductDto {

    @ApiModelProperty(notes = "Product id.", example = "e791fbe5-3bcf-40ab-aeff-621358b87259")
    String id;
    @ApiModelProperty(notes = "Product name.", example = "Test product name")
    String name;
    @ApiModelProperty(notes = "Product description.", example = "Test product description")
    String description;
    @ApiModelProperty(notes = "Product price.", example = "9.99")
    BigDecimal price;
    @ApiModelProperty(notes = "Currency code.", example = "EUR")
    String currency;
}
