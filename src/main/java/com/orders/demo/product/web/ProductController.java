package com.orders.demo.product.web;

import com.orders.demo.common.web.ControllerBase;
import com.orders.demo.common.Currency;
import com.orders.demo.common.Money;
import com.orders.demo.product.CreateProductCommand;
import com.orders.demo.product.ModifyProductCommand;
import com.orders.demo.product.ProductId;
import com.orders.demo.product.mapper.ProductMapper;
import com.orders.demo.product.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;

@RequiredArgsConstructor
@Api("Products endpoints.")
@RestController
@RequestMapping(value = "/products")
public class ProductController extends ControllerBase {

    private final ProductService productService;
    private final ProductMapper productMapper;

    @ApiOperation("List all products.")
    @GetMapping
    public List<ProductDto> findProducts() {
        return productService.findAllProducts().stream()
                .map(productMapper::toApi)
                .collect(Collectors.toList());
    }

    @ApiOperation("Get a single product.")
    @GetMapping("/{id}")
    public ProductDto getProduct(@PathVariable("id") final String productId) {
        return productMapper.toApi(productService.getActiveProductByOriginalId(getAudit(), new ProductId(productId)));
    }

    @ApiOperation("Create a product.")
    @Secured("admin")
    @PostMapping
    public ResponseEntity<ProductDto> createProduct(
            @ApiParam("Product details.")
            @Valid @RequestBody final CreateProductRequest createProductRequest) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(productMapper.toApi(
                        productService.createProduct(getAudit(), createProductCommand(createProductRequest))));
    }

    @ApiOperation("Update the specific product.")
    @Secured("admin")
    @PutMapping("/{id}")
    public ProductDto updateProduct(@PathVariable("id") final String productId,
            @ApiParam("Product details.")
            @Valid @RequestBody final CreateProductRequest updateProductRequest) {
        return productMapper
                .toApi(productService.updateProduct(getAudit(), new ProductId(productId),
                        modifyProductCommand(updateProductRequest)));
    }

    private CreateProductCommand createProductCommand(final CreateProductRequest request) {
        return new CreateProductCommand(request.getName(), request.getDescription(), new Money(request.getPrice(),
                Currency.valueOf(request.getCurrency())));
    }

    private ModifyProductCommand modifyProductCommand(final CreateProductRequest request) {
        return new ModifyProductCommand(request.getName(), request.getDescription(), new Money(request.getPrice(),
                Currency.valueOf(request.getCurrency())));
    }
}
