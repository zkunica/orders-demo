package com.orders.demo.product.web;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.NonNull;
import lombok.Value;

import java.math.BigDecimal;

@ApiModel(description = "Object to create or modify the product")
@Value
public final class CreateProductRequest {

    @ApiModelProperty(notes = "Name of the product.", example = "Test product", required = true)
    @NonNull
    String name;
    @ApiModelProperty(notes = "Description of the product.", example = "Test product details")
    String description;
    @ApiModelProperty(notes = "Price of the product.", example = "9.99", required = true)
    @NonNull
    BigDecimal price;
    @ApiModelProperty(notes = "Currency code of the product price.", example = "EUR", required = true)
    @NonNull
    String currency;
}
