package com.orders.demo.product;

import com.orders.demo.common.Audit;
import com.orders.demo.common.Money;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Value
public final class Product {

    @NonNull
    ProductId id;

    @NonNull
    ProductId originalId;

    @NonNull
    String name;

    @NonNull
    String description;

    @NonNull
    Money price;

    @NonNull
    Audit created;

    @With
    @NonNull
    Boolean active;

    public static Product create(final Audit audit, final String name, final String description, final Money price) {
        final ProductId productId = ProductId.nextId();
        if (!price.greaterThanZero()) {
            throw new IllegalArgumentException("Price must be greater than zero");
        }
        return new Product(
                productId,
                productId,
                name,
                description,
                price,
                audit,
                true
        );
    }

    public Product update(final Audit audit, final String name, final String description, final Money price) {
        return new Product(
                ProductId.nextId(),
                originalId,
                name,
                description,
                price,
                audit,
                true
        );
    }

    public Product deactivate() {
        return withActive(false);
    }
}
