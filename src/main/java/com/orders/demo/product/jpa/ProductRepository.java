package com.orders.demo.product.jpa;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface ProductRepository extends CrudRepository<ProductJpa, String> {

    @Query("select p from ProductJpa p where p.active = true")
    Stream<ProductJpa> findAllProducts();

    Optional<ProductJpa> findByOrOriginalIdAndActiveIsTrue(final String originalId);
}
