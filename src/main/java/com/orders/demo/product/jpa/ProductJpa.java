package com.orders.demo.product.jpa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "products")
public class ProductJpa {

    @Id
    String id;

    @Column(name = "original_id")
    @NotNull
    String originalId;

    @Column(name = "name")
    @NotNull
    String name;

    @Column(name = "description")
    @NotNull
    String description;

    @Column(name = "amount")
    @NotNull
    BigDecimal amount;

    @Column(name = "currency")
    @NotNull
    String currency;

    @Column(name = "active")
    @NotNull
    Boolean active;

    @Column(name = "created_at")
    Instant createdAt;

    @Column(name = "created_by")
    String createdBy;


}
