package com.orders.demo.product;

import com.orders.demo.common.EntityNotFoundException;

public class ProductNotFoundException extends EntityNotFoundException {

    public ProductNotFoundException(final ProductId productId) {
        super("Product " + productId + " was not found");
    }
}
