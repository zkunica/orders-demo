package com.orders.demo.product;

import com.orders.demo.common.Audit;
import com.orders.demo.product.jpa.ProductJpa;
import com.orders.demo.product.jpa.ProductRepository;
import com.orders.demo.product.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Transactional
    public Product createProduct(final Audit audit, final CreateProductCommand createProductCommand) {
        final Product product = Product.create(
                audit,
                createProductCommand.getName(),
                createProductCommand.getDescription(),
                createProductCommand.getPrice());
        final ProductJpa saved = productRepository.save(productMapper.toJpa(product));
        return productMapper.toModel(saved);
    }

    @Transactional
    public Product getProduct(final Audit audit, final ProductId productId) {
        return productRepository.findById(productId.getId()).map(productMapper::toModel)
                .orElseThrow(() -> new ProductNotFoundException(productId));
    }

    @Transactional
    public Product getActiveProductByOriginalId(final Audit audit, final ProductId originalId) {
        return productRepository.findByOrOriginalIdAndActiveIsTrue(originalId.getId()).map(productMapper::toModel)
                .orElseThrow(() -> new ProductNotFoundException(originalId));
    }

    @Transactional
    public Product updateProduct(final Audit audit, final ProductId productId,
            final ModifyProductCommand modifyProductCommand) {
        final Product previous = productRepository.findByOrOriginalIdAndActiveIsTrue(productId.getId())
                .map(productMapper::toModel)
                .orElseThrow(() -> new ProductNotFoundException(productId));
        final Product updatedProduct = previous.update(
                audit,
                modifyProductCommand.getName(),
                modifyProductCommand.getDescription(),
                modifyProductCommand.getPrice()
        );
        productRepository.save(productMapper.toJpa(previous.deactivate()));
        final ProductJpa saved = productRepository.save(productMapper.toJpa(updatedProduct));
        return productMapper.toModel(saved);
    }

    @Transactional
    public List<Product> findAllProducts() {
        return productRepository.findAllProducts()
                .map(productMapper::toModel)
                .collect(Collectors.toList());
    }

}
