package com.orders.demo.product;

import com.orders.demo.common.Money;
import lombok.NonNull;
import lombok.Value;

@Value
public final class ModifyProductCommand {

    @NonNull
    String name;

    String description;

    @NonNull
    Money price;

}
