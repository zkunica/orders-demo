package com.orders.demo.order;

import com.orders.demo.product.Product;
import lombok.Value;

import java.math.BigDecimal;

@Value
public final class ProductQuantity {

    Product product;
    BigDecimal quantity;
}
