package com.orders.demo.order.mapper;

import com.orders.demo.order.Order;
import com.orders.demo.order.OrderCustomerDetails;
import com.orders.demo.order.OrderId;
import com.orders.demo.order.OrderItem;
import com.orders.demo.order.OrderItemId;
import com.orders.demo.order.web.OrderDto;
import com.orders.demo.order.web.OrderItemDto;
import com.orders.demo.common.Audit;
import com.orders.demo.common.Currency;
import com.orders.demo.common.CustomerId;
import com.orders.demo.common.Money;
import com.orders.demo.order.jpa.OrderItemJpa;
import com.orders.demo.order.jpa.OrderJpa;
import com.orders.demo.product.Product;
import com.orders.demo.product.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@RequiredArgsConstructor
@Component
public class OrderMapper {

    private final ProductMapper productMapper;

    public OrderJpa toJpa(final Order model) {
        final OrderJpa orderJpa = new OrderJpa(
                model.getId().getId(),
                model.getCustomerDetails().getCustomerId().getId(),
                model.getCustomerDetails().getCustomerName(),
                model.getCustomerDetails().getCustomerEmail(),
                model.getStatus(),
                model.getTotalAmount().getAmount(),
                model.getTotalAmount().getCurrency().name(),
                model.getCreated().getInitiatedAt(),
                model.getCreated().getUserId(),
                ofNullable(model).map(Order::getUpdated).map(Audit::getInitiatedAt).orElse(null),
                ofNullable(model).map(Order::getUpdated).map(Audit::getUserId).orElse(null),
                ofNullable(model).map(Order::getPlaced).map(Audit::getInitiatedAt).orElse(null),
                ofNullable(model).map(Order::getPlaced).map(Audit::getUserId).orElse(null),
                ofNullable(model).map(Order::getCancelled).map(Audit::getInitiatedAt).orElse(null),
                ofNullable(model).map(Order::getCancelled).map(Audit::getUserId).orElse(null),
                null);
        orderJpa.setItems(model.getItems().stream()
                .map(orderItem -> toJpa(orderJpa, orderItem))
                .collect(Collectors.toList()));
        return orderJpa;
    }

    public OrderItemJpa toJpa(final OrderJpa orderJpa, final OrderItem model) {
        return new OrderItemJpa(
                model.getId().getId(),
                orderJpa,
                productMapper.toJpa(model.getProduct()),
                model.getQuantity(),
                model.getTotalAmount().getAmount()
        );
    }

    public OrderItemDto toApi(final OrderItem model) {
        return new OrderItemDto(
                model.getId().getId(),
                productMapper.toApi(model.getProduct()),
                model.getQuantity(),
                model.getTotalAmount().getAmount()
        );
    }

    public OrderDto toApi(final Order model) {
        return new OrderDto(
                model.getId().getId(),
                model.getCustomerDetails().getCustomerName(),
                model.getCustomerDetails().getCustomerEmail(),
                model.getTotalAmount().getAmount(),
                model.getTotalAmount().getCurrency().name(),
                model.getPlaced().getInitiatedAt(),
                model.getItems().stream().map(this::toApi).collect(Collectors.toList()));
    }

    public Order toModel(final OrderJpa jpa) {
        return new Order(
                new OrderId(jpa.getId()),
                new OrderCustomerDetails(new CustomerId(jpa.getCustomerId()), jpa.getCustomerName(),
                        jpa.getCustomerEmail()),
                jpa.getStatus(),
                mapAudit(jpa.getCreatedBy(), jpa.getCreatedAt(), false),
                mapAudit(jpa.getUpdatedBy(), jpa.getUpdatedAt(), true),
                mapAudit(jpa.getPlacedBy(), jpa.getPlacedAt(), true),
                mapAudit(jpa.getCancelledBy(), jpa.getCancelledAt(), true),
                new Money(jpa.getAmount(), Currency.valueOf(jpa.getCurrency())),
                jpa.getItems().stream().map(this::toModel).collect(Collectors.toList())
        );
    }

    public OrderItem toModel(final OrderItemJpa jpa) {
        final Product product = productMapper.toModel(jpa.getProductJpa());
        return new OrderItem(
                new OrderItemId(jpa.getId()),
                product,
                jpa.getQuantity(),
                new Money(jpa.getAmount(), product.getPrice().getCurrency())
        );
    }

    private Audit mapAudit(final String initiatedBy, final Instant initiatedAt, final boolean nullable) {
        if (nullable && initiatedAt == null && initiatedBy == null) {
            return null;
        }
        return new Audit(initiatedBy, initiatedAt);
    }
}
