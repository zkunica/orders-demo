package com.orders.demo.order.jpa;

import com.orders.demo.product.jpa.ProductJpa;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "order_items")
public class OrderItemJpa {

    @Id
    String id;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private OrderJpa order;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private ProductJpa productJpa;

    @Column(name = "quantity")
    @NotNull
    BigDecimal quantity;

    @Column(name = "amount")
    @NotNull
    BigDecimal amount;
}
