package com.orders.demo.order.jpa;

import com.orders.demo.order.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class OrderJpa {

    @Id
    String id;

    @Column(name = "customer_id")
    @NotNull
    String customerId;

    @Column(name = "customer_name")
    @NotNull
    String customerName;

    @Column(name = "customer_email")
    @NotNull
    String customerEmail;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    OrderStatus status;

    @Column(name = "amount")
    @NotNull
    BigDecimal amount;

    @Column(name = "currency")
    @NotNull
    String currency;

    @Column(name = "created_at")
    Instant createdAt;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "updated_at")
    Instant updatedAt;

    @Column(name = "updated_by")
    String updatedBy;

    @Column(name = "placed_at")
    Instant placedAt;

    @Column(name = "placed_by")
    String placedBy;

    @Column(name = "cancelled_at")
    Instant cancelledAt;

    @Column(name = "cancelled_by")
    String cancelledBy;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, targetEntity = OrderItemJpa.class, cascade = CascadeType.ALL)
    @OrderBy(value = "id")
    List<OrderItemJpa> items;
}
