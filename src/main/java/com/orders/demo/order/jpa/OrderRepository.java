package com.orders.demo.order.jpa;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface OrderRepository extends CrudRepository<OrderJpa, String> {

    @Query("select o from OrderJpa o where o.placedAt between :placedFrom and :placedTo")
    Stream<OrderJpa> findAllByPlacedAtBetween(@Param("placedFrom") final Instant placedFrom,
            @Param("placedTo") final Instant placedTo);

    Stream<OrderJpa> findAllByCustomerIdAndPlacedAtBetween(final String customerId, final Instant placedFrom,
            final Instant placedTo);

    Optional<OrderJpa> findOrderJpaByIdAndCustomerId(final String id, final String customerId);
}
