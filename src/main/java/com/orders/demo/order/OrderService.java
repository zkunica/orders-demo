package com.orders.demo.order;

import com.orders.demo.common.Audit;
import com.orders.demo.common.CustomerId;
import com.orders.demo.order.jpa.OrderJpa;
import com.orders.demo.order.jpa.OrderRepository;
import com.orders.demo.order.mapper.OrderMapper;
import com.orders.demo.product.Product;
import com.orders.demo.product.ProductId;
import com.orders.demo.product.mapper.ProductMapper;
import com.orders.demo.product.ProductNotFoundException;
import com.orders.demo.product.jpa.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final OrderMapper orderMapper;
    private final ProductMapper productMapper;

    @Transactional
    public Order placeOrder(final Audit audit, final CreateOrderCommand createOrderCommand) {
        final Order order = Order.empty(
                audit,
                new OrderCustomerDetails(
                        createOrderCommand.getCustomerId(),
                        createOrderCommand.getCustomerName(),
                        createOrderCommand.getCustomerEmail()))
                .addItems(createOrderCommand.getItems()
                        .stream()
                        .map(this::createProductQuantity)
                        .collect(Collectors.toList()))
                .place(audit);
        final OrderJpa saved = orderRepository.save(orderMapper.toJpa(order));
        return orderMapper.toModel(saved);
    }

    @Transactional
    public List<Order> findAllOrders(final Instant placedFrom, final Instant placedTo) {
        return orderRepository.findAllByPlacedAtBetween(placedFrom, placedTo)
                .map(orderMapper::toModel)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<Order> findAllOrdersByCustomerId(final CustomerId customerId, final Instant placedFrom,
            final Instant placedTo) {
        return orderRepository.findAllByCustomerIdAndPlacedAtBetween(customerId.getId(), placedFrom, placedTo)
                .map(orderMapper::toModel)
                .collect(Collectors.toList());
    }

    @Transactional
    public Order getOrder(final OrderId orderId) {
        return orderRepository.findById(orderId.getId())
                .map(orderMapper::toModel)
                .orElseThrow(() -> new IllegalArgumentException("Order not found"));
    }

    @Transactional
    public Order getOrderByIdAndCustomerId(final OrderId orderId, final CustomerId customerId) {
        return orderRepository.findOrderJpaByIdAndCustomerId(orderId.getId(), customerId.getId())
                .map(orderMapper::toModel)
                .orElseThrow(() -> new IllegalArgumentException("Order not found"));
    }

    private ProductQuantity createProductQuantity(final CreateOrderItemCommand command) {
        final Product product = productMapper
                .toModel(productRepository.findByOrOriginalIdAndActiveIsTrue(command.getProductId().getId())
                        .orElseThrow(() -> new ProductNotFoundException(new ProductId(command.getProductId().getId()))));
        return new ProductQuantity(product, command.getQuantity());
    }

}
