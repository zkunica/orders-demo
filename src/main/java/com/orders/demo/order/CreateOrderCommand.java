package com.orders.demo.order;

import com.orders.demo.common.CustomerId;
import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
public final class CreateOrderCommand {

    @NonNull
    CustomerId customerId;

    String customerName;

    @NonNull
    String customerEmail;

    @NonNull
    List<CreateOrderItemCommand> items;
}
