package com.orders.demo.order;

import com.orders.demo.common.CustomerId;
import lombok.NonNull;
import lombok.Value;

@Value
public final class OrderCustomerDetails {

    @NonNull
    CustomerId customerId;
    String customerName;
    @NonNull
    String customerEmail;
}
