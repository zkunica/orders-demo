package com.orders.demo.order;

import com.orders.demo.common.EntityNotFoundException;

public class OrderNotFoundException extends EntityNotFoundException {

    public OrderNotFoundException(final OrderId orderId) {
        super("Order " + orderId + " was not found");
    }
}
