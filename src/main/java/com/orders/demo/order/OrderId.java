package com.orders.demo.order;

import lombok.Value;

import java.util.UUID;

@Value
public final class OrderId {

    String id;

    public static OrderId nextId() {
        return new OrderId(UUID.randomUUID().toString());
    }
}
