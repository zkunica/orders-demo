package com.orders.demo.order;

import com.orders.demo.product.ProductId;
import lombok.NonNull;
import lombok.Value;

import java.math.BigDecimal;

@Value
public final class CreateOrderItemCommand {

    @NonNull
    ProductId productId;

    @NonNull
    BigDecimal quantity;
}
