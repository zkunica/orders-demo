package com.orders.demo.order;

import com.orders.demo.common.Money;
import com.orders.demo.product.Product;
import lombok.Value;

import java.math.BigDecimal;
import java.util.Optional;

@Value
public final class OrderItem {

    OrderItemId id;

    Product product;

    BigDecimal quantity;

    Money totalAmount;

    public static OrderItem createForProductQuantity(final ProductQuantity productQuantity) {
        return createForProduct(productQuantity.getProduct(), productQuantity.getQuantity());
    }

    public static OrderItem createForProduct(final Product product, final BigDecimal quantity) {
        if (product == null) {
            throw new IllegalArgumentException("Product must be specified");
        }
        if (Optional.ofNullable(quantity).orElse(BigDecimal.ZERO).compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Quantity must be set");
        }
        return new OrderItem(
                OrderItemId.nextId(),
                product,
                quantity,
                product.getPrice().multiply(quantity)
        );
    }
}
