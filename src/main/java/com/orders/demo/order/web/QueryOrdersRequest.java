package com.orders.demo.order.web;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;

@ApiModel(description = "Orders query filter.")
@Value
public final class QueryOrdersRequest {

    @ApiModelProperty(allowEmptyValue = true, notes = "Placed after", example = "2019-11-01T00:00:00.000Z")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    Instant placedFrom;
    @ApiModelProperty(allowEmptyValue = true, notes = "Placed until", example = "2019-11-30T00:00:00.000Z")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    Instant placedTo;
}
