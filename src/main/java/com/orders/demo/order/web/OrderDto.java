package com.orders.demo.order.web;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@ApiModel(description = "Order.")
@Value
public final class OrderDto {

    @ApiModelProperty(notes = "Unique identifier of the order.")
    String id;
    @ApiModelProperty(notes = "Name of the customer.")
    String customerName;
    @ApiModelProperty(notes = "Customer's email.")
    String customerEmail;
    @ApiModelProperty(notes = "Total order amount.")
    BigDecimal totalAmount;
    @ApiModelProperty(notes = "Currency code.")
    String currency;
    @ApiModelProperty(notes = "Time when the order was placed.")
    Instant placedAt;
    @ApiModelProperty(notes = "Order items.")
    List<OrderItemDto> items;
}
