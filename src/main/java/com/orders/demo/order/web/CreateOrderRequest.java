package com.orders.demo.order.web;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.NonNull;
import lombok.Value;

import java.util.List;

@ApiModel(description = "Object to create an order")
@Value
public final class CreateOrderRequest {

    @ApiModelProperty(notes = "Id of the customer.", example = "1", required = true)
    @NonNull
    String customerId;
    @ApiModelProperty(notes = "Customer name.", example = "Johny Cash")
    String customerName;
    @ApiModelProperty(notes = "Customer email", example = "cash@test.com")
    @NonNull
    String customerEmail;
    @ApiModelProperty(notes = "Order items")
    @NonNull
    List<CreateOrderItemRequest> items;
}
