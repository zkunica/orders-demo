package com.orders.demo.order.web;

import com.orders.demo.product.web.ProductDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import java.math.BigDecimal;

@ApiModel(description = "Order item.")
@Value
public final class OrderItemDto {

    @ApiModelProperty(notes = "Unique item identifier.")
    String id;
    @ApiModelProperty(notes = "Product details.")
    ProductDto product;
    @ApiModelProperty(notes = "Quantity.")
    BigDecimal quantity;
    @ApiModelProperty(notes = "Total item amount.")
    BigDecimal totalAmount;
}
