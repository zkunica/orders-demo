package com.orders.demo.order.web;

import com.orders.demo.common.CustomerId;
import com.orders.demo.common.User;
import com.orders.demo.common.web.ControllerBase;
import com.orders.demo.order.CreateOrderCommand;
import com.orders.demo.order.CreateOrderItemCommand;
import com.orders.demo.order.Order;
import com.orders.demo.order.OrderId;
import com.orders.demo.order.OrderService;
import com.orders.demo.order.mapper.OrderMapper;
import com.orders.demo.product.ProductId;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;

import static java.util.Optional.ofNullable;

@RequiredArgsConstructor
@Api("Orders endpoints.")
@RestController
@RequestMapping(value = "/orders")
public class OrderController extends ControllerBase {

    private final OrderService orderService;
    private final OrderMapper orderMapper;

    @ApiOperation("Query all orders based on query.")
    @GetMapping
    public List<OrderDto> findOrders(@ApiParam("Orders filtering query.") final QueryOrdersRequest query) {
        final User user = getUser();
        final List<Order> orders =
                user.isAdmin()
                        ? orderService.findAllOrders(
                        ofNullable(query.getPlacedFrom()).orElse(Instant.EPOCH),
                        ofNullable(query.getPlacedTo()).orElse(Instant.now()))
                        : orderService.findAllOrdersByCustomerId(new CustomerId(user.getId()),
                                ofNullable(query.getPlacedFrom()).orElse(Instant.EPOCH),
                                ofNullable(query.getPlacedTo()).orElse(Instant.now()));
        return orders.stream()
                .map(orderMapper::toApi)
                .collect(Collectors.toList());
    }

    @ApiOperation("Get a single order.")
    @GetMapping("/{id}")
    public OrderDto getOrder(@PathVariable("id") final String orderId) {
        final User user = getUser();
        final Order order = user.isAdmin()
                ? orderService.getOrder(new OrderId(orderId))
                : orderService.getOrderByIdAndCustomerId(new OrderId(orderId), new CustomerId(
                        user.getId()));
        return orderMapper.toApi(order);
    }

    @ApiOperation("Create order.")
    @PostMapping
    public ResponseEntity<OrderDto> createOrder(
            @ApiParam("Order details.")
            @Valid @RequestBody final CreateOrderRequest createOrderRequest) {
        final User user = getUser();
        if (!user.isAdmin() && !user.getId().equals(createOrderRequest.getCustomerId())) {
            throw new IllegalArgumentException("Not allowed to create order for another customer");
        }
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(orderMapper.toApi(
                        orderService.placeOrder(getAudit(), createOrderCommand(createOrderRequest))));
    }

    private CreateOrderCommand createOrderCommand(final CreateOrderRequest createOrderRequest) {
        return new CreateOrderCommand(
                new CustomerId(createOrderRequest.getCustomerId()),
                createOrderRequest.getCustomerName(),
                createOrderRequest.getCustomerEmail(),
                createOrderRequest.getItems().stream()
                        .map(createOrderItemRequest -> new CreateOrderItemCommand(
                                new ProductId(createOrderItemRequest.getProductId()),
                                createOrderItemRequest.getQuantity()))
                        .collect(Collectors.toList())
        );
    }

}
