package com.orders.demo.order.web;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.NonNull;
import lombok.Value;

import java.math.BigDecimal;

@ApiModel(description = "Object to create one order item")
@Value
public final class CreateOrderItemRequest {

    @ApiModelProperty(notes = "Id of the product", example = "46b8686d-0d9d-4578-833f-daf7d1e9121a")
    @NonNull
    String productId;

    @ApiModelProperty(notes = "Quantity of the product to place on order", example = "0.5")
    @NonNull
    BigDecimal quantity;
}
