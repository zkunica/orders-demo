package com.orders.demo.order;

public enum OrderStatus {
    CREATED,
    PLACED,
    CANCELLED
}
