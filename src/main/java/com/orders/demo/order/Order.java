package com.orders.demo.order;

import com.orders.demo.common.Audit;
import com.orders.demo.common.Money;
import com.orders.demo.product.Product;
import lombok.NonNull;
import lombok.Value;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Value
public final class Order {

    @NonNull
    OrderId id;

    @NonNull
    OrderCustomerDetails customerDetails;

    @NonNull
    OrderStatus status;

    @NonNull
    Audit created;

    Audit updated;

    Audit placed;

    Audit cancelled;

    @NonNull
    Money totalAmount;

    @NonNull
    List<OrderItem> items;

    public static Order empty(final Audit audit, final OrderCustomerDetails orderCustomerDetails) {
        return new Order(
                OrderId.nextId(),
                orderCustomerDetails,
                OrderStatus.CREATED,
                audit,
                audit,
                null,
                null,
                Money.empty(),
                Collections.emptyList());
    }

    public Order addItem(final Product product, final BigDecimal quantity) {
        final List<OrderItem> updatedItems = new ArrayList<>();
        updatedItems.addAll(items);
        final OrderItem itemToAdd = OrderItem.createForProduct(product, quantity);
        updatedItems.add(itemToAdd);
        return updateItems(updatedItems);
    }

    public Order addItems(final List<ProductQuantity> orderItems) {
        final List<OrderItem> updatedItems = new ArrayList<>();
        updatedItems.addAll(items);
        updatedItems.addAll(orderItems.stream()
                .map(OrderItem::createForProductQuantity)
                .collect(Collectors.toList()));
        return updateItems(updatedItems);
    }

    public Order place(final Audit audit) {
        if (items.isEmpty()) {
            throw new IllegalArgumentException("Cannot place empty order");
        }
        return new Order(
                id,
                customerDetails,
                OrderStatus.PLACED,
                created,
                updated,
                audit,
                cancelled,
                totalAmount,
                items);
    }

    private Order updateItems(final List<OrderItem> updatedItems) {
        final Money updatedTotalAmount = calculateTotal(updatedItems);
        return new Order(
                id,
                customerDetails,
                status,
                created,
                updated,
                placed,
                cancelled,
                updatedTotalAmount,
                updatedItems);
    }

    private Money calculateTotal(List<OrderItem> items) {
        return items.stream()
                .map(OrderItem::getTotalAmount)
                .reduce(Money.empty(), Money::add);
    }
}
