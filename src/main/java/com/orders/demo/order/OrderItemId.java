package com.orders.demo.order;

import lombok.Value;

import java.util.UUID;

@Value
public final class OrderItemId {

    String id;

    public static OrderItemId nextId() {
        return new OrderItemId(UUID.randomUUID().toString());
    }
}
