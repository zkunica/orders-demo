package com.orders.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.orders.demo" })
public class ServiceOrdersApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceOrdersApplication.class, args);
    }

}
