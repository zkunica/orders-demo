CREATE TABLE orders
(
    id             VARCHAR(255),
    customer_id    VARCHAR(255) NOT NULL,
    customer_name  VARCHAR(255) NOT NULL,
    customer_email VARCHAR(255) NOT NULL,
    status         VARCHAR(50),
    amount         decimal      not null,
    currency       VARCHAR(10)  not null,
    created_at     TIMESTAMP    NOT NULL,
    created_by     VARCHAR(255) NOT NULL,
    updated_at     TIMESTAMP,
    updated_by     VARCHAR(255),
    placed_at      TIMESTAMP,
    placed_by      VARCHAR(255),
    cancelled_at   TIMESTAMP,
    cancelled_by   VARCHAR(255),
    CONSTRAINT orders_pk PRIMARY KEY (id)
);

CREATE INDEX i_orders_customer ON orders (customer_id);
CREATE INDEX i_orders_placed_at ON orders (placed_at);

CREATE TABLE order_items
(
    id         VARCHAR(255) NOT NULL,
    order_id   VARCHAR(255) NOT NULL,
    product_id VARCHAR(255) NOT NULL,
    quantity   decimal      not null,
    amount     decimal      not null,
    CONSTRAINT order_items_pk PRIMARY KEY (id),
    FOREIGN KEY (order_id) REFERENCES orders (id),
    FOREIGN KEY (product_id) REFERENCES products (id)
);
CREATE INDEX i_order_items_order ON order_items (order_id);
CREATE INDEX i_order_items_product ON order_items (product_id);