CREATE TABLE products
(
    id          VARCHAR(255) NOT NULL,
    original_id VARCHAR(255) NOT NULL,
    name        VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    amount      decimal      not null,
    currency    VARCHAR(10)  not null,
    created_at  TIMESTAMP    NOT NULL,
    created_by  VARCHAR(255) NOT NULL,
    active      bool         NOT NULL,
    CONSTRAINT products_pk PRIMARY KEY (id)
);
CREATE INDEX i_products_name ON products (name);
CREATE INDEX i_products_original_id ON products (original_id);
CREATE INDEX i_products_active ON products (active);
