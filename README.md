# Orders - demo service

## Use case
Service to handle CRUD operations of products that have price and name.
Also allow placing customer orders with multiple products and calculated totals.
Changing products should not change already placed orders.

## Approach
### Products
Products are implemented in a way that every change is a new record in the database. Product is tracked by originalId and active properties.
Whenever a product is updated the active record is marked as inactive and a new one is created with the new id, but still keeping the same originalId.
This way we can reference product from orders without being affected by the product change.

### Orders
Orders have one or more items that reference a product at the time of creation and quantity of the products.
For simplicity orders can only be placed and not changed or cancelled.

### Security
Considering simplicity for this case it is assumed that authentication is being handled before reaching the service.
Usually in the container orchestrated environment with multiple micro services the specific service would be
hidden from the public and accessible only after the user request has passed the authentication.
One of the popular approaches would also be JWT token authentication which is practical as the same token
is passed around various services and each service then verifies it's validity and can read permissions from it. In that case
the service can be exposed to the public and there is no need for a proxy between the client and the services. 

In this case service expects the result of the authentication to be presented as a http header x-api-key with the value `<user-id>;<permissions>`:

`x-api-key`: `543;customer`

User authenticated as customer can:
- list products
- create own order
- list own orders
   
User authenticated as admin can:
- list products
- add products
- modify products
- create customer orders
- list all orders

### Architecture
This service is an example of a micro service. But for the simplicity of the case products and orders are both being handled by this service (not a good approach for micro services :).
In a real case scenario those should be presented by two separate micro services.
Current implementation consists of a single service with postgres database for the storage.

Running multiple instance of the service would require some container orchestration implementation along with load balancer.
If we had more than one micro service is our case, instead of using local transaction on a single database, we'd need to use 
distributed transaction flow, including asynchronous messaging between services. In such case some queue/topic implementation
would be need (Kafka, SNS - SQS, ...)

## Technology

<b>Postgres</b> database is used for this case.
Service is implemented using <b>Spring Boot 2.2.1</b> and <b>Java 11</b>. 
Build is done by <b>Maven</b>, however it is not required to be installed for running the service.

## Requirements

To run the service we need:
- Docker (https://www.docker.com/)
- docker-compose (https://github.com/docker/compose)

## Running the service
1. clone the repository
2. build the images ``` docker-compose build```
3. run everything ```docker-compose up -d```

The service will be available at http://localhost:8180

## Building the service without docker
### Requirements
- Maven
- JDK 11+
- (optional) SonarQube https://www.sonarqube.org/

### Building the service
1. clone the repository
2. Build and run tests: `mvn clean package`
3. SonarQube validation (Sonar needs to be running): `mvn clean verify sonar:sonar`  

## Swagger
Service comes with swagger documentation. Swagger UI is available at http://localhost:8180/swagger-ui.html
For authentication api key we can use values like <customer-id>;<comma-separated-permissions>

Example for admin user: `admin;admin` 

Example for customer user: `123;customer`

## Endpoints

### Products
- list all products:
 
```
curl -X GET \
     http://localhost:8180/products \
     -H 'Accept: */*' \
     -H 'Accept-Encoding: gzip, deflate' \
     -H 'Content-Type: application/json' \
     -H 'cache-control: no-cache' \
     -H 'x-api-key: 1;customer' 
```
    
example response:

```
[
       {
           "id": "e791fbe5-3bcf-40ab-aeff-621358b87259",
           "name": "Leather glowes",
           "description": "Brown leather glowes",
           "price": 145.89,
           "currency": "EUR"
       }
   ]
```
- create a product:

```
curl -X POST \
http://localhost:8180/products \
-H 'Accept: */*' \
-H 'Accept-Encoding: gzip, deflate' \
-H 'Content-Type: application/json' \
-H 'cache-control: no-cache' \
-H 'x-api-key: 1;admin' \
-d ' {
"name": "White shirt",
  "description": "Plain white shirt",
  "price": 9.99,
  "currency": "EUR"
}'
```
  
example response:
  
```
{
    "id": "4f3a029d-067f-401b-9139-c99bfae3a2aa",
    "name": "White shirt",
    "description": "Plain white shirt",
    "price": 9.99,
    "currency": "EUR"
}
```
  
- update a product

```
curl -X PUT \
  http://localhost:8180/products/e791fbe5-3bcf-40ab-aeff-621358b87259 \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -H 'x-api-key: 1;admin' \
  -d ' {
 	"name": "Black belt",
    "description": "Black belt",
    "price": 29.00,
    "currency": "EUR"
 }'
```

example response:

```
{
    "id": "e791fbe5-3bcf-40ab-aeff-621358b87259",
    "name": "Black belt",
    "description": "Black belt",
    "price": 29.00,
    "currency": "EUR"
}

``` 

### Orders
- list all orders: 

```
curl -X GET \
    http://localhost:8180/orders?placedFrom=2019-11-01T00%3A00%3A00.000Z&placedTo=2019-11-30T00%3A00%3A00.000Z \ 
    -H "Accept: */*" \
    -H 'Accept-Encoding: gzip, deflate' \
    -H 'Content-Type: application/json' \
    -H 'cache-control: no-cache' \
    -H "x-api-key: 1;customer"
```

example response:


- place order

```
curl -X \
    POST "http://localhost:8180/orders" \
    -H "accept: */*" \ 
    -H "x-api-key: 1;customer" 
    -H "Content-Type: application/json" \
    -d "{ 
        "customerEmail": "cash@test.com", 
        "customerId": 1, 
        "customerName": "Johny Cash", 
        "items": [ 
            { 
                "productId": "4f3a029d-067f-401b-9139-c99bfae3a2aa", 
                "quantity": 2 
            } 
        ]}"
```

example response:

```
{
  "id": "4818976f-1976-4ee2-a0b9-608ea29137cb",
  "customerName": "Johny Cash",
  "customerEmail": "cash@test.com",
  "totalAmount": 19.98,
  "currency": "EUR",
  "placedAt": "2019-11-16T18:25:37.678343Z",
  "items": [
    {
      "id": "d3ef4ebc-4f33-4a7f-a809-d68f4a27079e",
      "product": {
        "id": "4f3a029d-067f-401b-9139-c99bfae3a2aa",
        "name": "White shirt",
        "description": "Plain white shirt",
        "price": 9.99,
        "currency": "EUR"
      },
      "quantity": 2,
      "totalAmount": 19.98
    }
  ]
}
```
